#include <iostream>
#include <vector>

using namespace std;

typedef struct node_structure
{
    int             data;
    node_structure *next;
}node_struct;

class LinkedList
{
    private:
        node_struct *head;
        node_struct* createNode(int num)
        {
            node_struct *newNode = new node_struct();
            newNode->data        = num;
            newNode->next        = NULL;
            return newNode;
        }

        void createLinkedList(vector<int> arr)
        {
            int arraySize = (int)arr.size();
            node_struct *movingNode = NULL;
            for(int i = 0 ; i < arraySize ; i++)
            {
                node_struct *newNode = createNode(arr[i]);
                if(!movingNode)
                {
                    movingNode = newNode;
                    head       = newNode;
                }
                else
                {
                    movingNode->next = newNode;
                    movingNode       = newNode;
                }
            }
        }
        
    public:
        LinkedList(vector<int> arr)
        {
            head = NULL;
            createLinkedList(arr);
        }

        node_struct* getLinkedList()
        {
            return head;
        }
        
        void displayList(node_struct *listHeadPtr)
        {
            while (listHeadPtr)
            {
                cout<<listHeadPtr->data<<" ";
                listHeadPtr = listHeadPtr->next;
            }
            cout<<endl;
        }
};

class Engine
{
    private:
        node_struct *headPtrOfList;
        int          intervalNode;
        
    public:
        Engine(node_struct *head , int iNode)
        {
            headPtrOfList = head;
            intervalNode  = iNode;
        }

        node_struct* reverseLinkedList()
        {
            int          counter          = 1;
            node_struct *innerHeadNode    = headPtrOfList;
            node_struct *innerMovingNode  = headPtrOfList;
            node_struct *innerCurrentNode = headPtrOfList->next;
            node_struct *innerLastNode    = NULL;
            while(innerCurrentNode)
            {
                if(!((counter % intervalNode) ))
                {
                    innerLastNode    = innerMovingNode;
                    if(counter == intervalNode)
                    {
                        headPtrOfList = innerHeadNode;
                    }
                    if(counter > 1)
                    {
                        innerHeadNode = innerCurrentNode;
                    }
                    innerMovingNode  = innerHeadNode;
                    innerCurrentNode = innerMovingNode->next;
                    ++counter;
                }
                innerMovingNode->next  = innerCurrentNode->next;
                innerCurrentNode->next = innerHeadNode;
                innerHeadNode          = innerCurrentNode;
                if(innerLastNode)
                {
                    innerLastNode->next = innerHeadNode;
                }
                innerCurrentNode       = innerMovingNode->next;
                ++counter;
            }
            return headPtrOfList;
        }
};


int main(int argc, const char * argv[])
{
    vector<int> arr = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    LinkedList ll   = LinkedList(arr);
    Engine e        = Engine(ll.getLinkedList() , 4);
    ll.displayList(e.reverseLinkedList());
    return 0;
}
